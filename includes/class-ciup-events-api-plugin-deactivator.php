<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.leadoff.fr
 * @since      1.0.0
 *
 * @package    Ciup_Events_Api
 * @subpackage Ciup_Events_Api/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Ciup_Events_Api
 * @subpackage Ciup_Events_Api/includes
 * @author     LEAD OFF <help@leadoff.io>
 */
class Ciup_Events_Api_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
