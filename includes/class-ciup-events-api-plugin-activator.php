<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.leadoff.fr
 * @since      1.0.0
 *
 * @package    Ciup_Events_Api
 * @subpackage Ciup_Events_Api/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Ciup_Events_Api
 * @subpackage Ciup_Events_Api/includes
 * @author     LEAD OFF <help@leadoff.io>
 */
class Ciup_Events_Api_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
