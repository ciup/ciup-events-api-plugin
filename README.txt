=== CIUP Events API ===
Contributors: (this should be a list of wordpress.org userid's)
Tags: ciup, api, events
Requires at least: 5.0.0
Tested up to: 5.3.2
Stable tag: 5.3.2
License: CC BY-NC-SA 4.0
License URI: https://creativecommons.org/licenses/by-nc-sa/4.0/

CIUP Events API is a Wordpress plugin. CIUP Events API is our way to send data from the CIUP website, you can put some filters like places and themes, number of post to show...

== Description ==

CIUP Events API is a Wordpress plugin. CIUP Events API is our way to send data from the CIUP website, you can put some filters like places and themes, number of post to show...

== Installation ==

1. Upload `ciup-api` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Need some help ? Check the CIUP Settings page in WP Settings.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 1.0 =
* First release.

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.

== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`