<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.leadoff.fr
 * @since      1.0.0
 *
 * @package    Ciup_Events_Api
 * @subpackage Ciup_Events_Api/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Ciup_Events_Api
 * @subpackage Ciup_Events_Api/public
 * @author     LEAD OFF <help@leadoff.io>
 */
class Ciup_Events_Api_Public
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ciup_Events_Api_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ciup_Events_Api_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/ciup-events-api-plugin-public.css', array(), $this->version, 'all' );
		wp_enqueue_style('ciup-events-api-lib-css', plugin_dir_url(__FILE__) . 'lib/ciup-events-api/ciup-events-api.min.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ciup_Events_Api_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ciup_Events_Api_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/ciup-events-api-plugin-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script('ciup-events-api-lib-js', plugin_dir_url(__FILE__) . 'lib/ciup-events-api/ciup-events-api.min.js', array('jquery'), $this->version, false);
	}

	/**
	 * Register the shortcode
	 *
	 * @since    1.0.0
	 */
	public function register_shortcodes()
	{

		add_shortcode('ciup', array($this, 'display_shortcode'));
	}

	// Display in front the result of the call to the api.
	public function display_shortcode($attr)
	{

		extract(shortcode_atts(array(
			'header' => true,
			'footer' => true,
			'layout' => 'default',
			'elements' => 4,
			'place' => '',
			'theme' => '',
			'period' => 'future'
		), $attr));

		$nb_random = random_int(1, 1000);
?>
		<div class="render-default-<?php echo $nb_random; ?>"></div>
		<script>
			var c<?php echo $nb_random; ?> = new Ciup({
				"header": <?php echo $attr['header']; ?>,
				"footer": <?php echo $attr['footer']; ?>,
				"layout": "<?php echo $attr['layout']; ?>",
				"elements": <?php echo $attr['elements']; ?>,
				"place": ["<?php echo $attr['place']; ?>"],
				"theme": ["<?php echo $attr['theme']; ?>"],
				"period": "<?php echo $attr['period']; ?>"
			});

			c<?php echo $nb_random; ?>.render('.render-default-<?php echo $nb_random; ?>');
		</script>
<?php
	}
}
