(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$( window ).load(function() {
		// Open modal
		$('.modal-ciup-button').on('click', function() {
			$('#modal-ciup').toggleClass('active');
		});

		// Close modal
		$('.modal-ciup-close').on('click', function() {
			$('#modal-ciup').removeClass('active');
		});

		// Check if click outside of the content
		$(window).on('click',function(event) {
		  if (event.target == $('#modal-ciup')[0]) {
		    $('#modal-ciup').removeClass('active');
		  }
		});

		// On click create the shortcode with ajax
		$('.create-shortcode').on('click',function(event) {

			$('.shortcode-result').empty();
			$('.ciup-shortcode-result').hide();

			var args = {
				"header": true,
				"footer": true,
				"layout": "default",
				"elements": 3,
				"place": "",
				"theme": "",
				"period": "future"
			};
			
		  	args.header = $('#header').val();
		  	args.footer = $('#footer').val();
		  	args.layout = $('#layout').val();
		  	args.elements = $('#elements').val();
		  	args.place = $('#place').val();
		  	args.theme = $('#theme').val();
		  	args.period = $('#period').val();

		  	$.ajax({
			    type: 'GET',
			    url: adminAjax,
			    dataType: 'text',
			    data: {
			    	action: 'create_the_shortcode',
			    	params: args
			    },
			    success: function (res) {
			    	$('.shortcode-result').html(res);
	            },
	            error: function (error) {
	            	console.error('There was a problem with the Ajax call !');
	            }
			}).then(function(res){
				$('.ciup-shortcode-result').show();
			});
		});

		// On click add in TinyMCE the shortcode where the cursor is
		$('.insert-shortcode').on('click', function(event) {
			var shortcode_text = $('.shortcode-result').text();
			tinymce.activeEditor.execCommand('mceInsertContent', false, "<p>"+shortcode_text+"</p>");
			$('#modal-ciup').removeClass('active');
		});

		$(".collapsible-button").on("click", function() {
			$(this).siblings().toggleClass("active");
		});
		
	});

})( jQuery );
