<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.leadoff.fr
 * @since      1.0.0
 *
 * @package    Ciup_Events_Api
 * @subpackage Ciup_Events_Api/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Ciup_Events_Api
 * @subpackage Ciup_Events_Api/admin
 * @author     LEAD OFF <help@leadoff.io>
 */
class Ciup_Events_Api_Admin
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function _construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ciup_Events_Api_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ciup_Events_Api_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style($this->plugin_name . '-css', plugin_dir_url(__FILE__) . 'css/ciup-events-api-plugin-admin.css', array(), $this->version, 'all');
		wp_enqueue_style($this->plugin_name . '-font-css', plugin_dir_url(__FILE__) . 'css/ciup-events-api-plugin-admin-font.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Ciup_Events_Api_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Ciup_Events_Api_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script($this->plugin_name . '-js', plugin_dir_url(__FILE__) . 'js/ciup-events-api-plugin-admin.min.js', array('jquery'), $this->version, false);
		// Create var for the Ajax call
		wp_localize_script($this->plugin_name . '-js', 'adminAjax', admin_url('admin-ajax.php'));
	}

	/**
	 * Add the ciup url
	 *
	 * @since    1.0.0
	 */
	public function add_constants()
	{

		// Define global constants
		if (!defined('CIUP_URL')) {
			define('CIUP_URL', 'http://www.citescope.fr/');
		}
	}

	/**
	 * Add the option page in the WP Settings
	 *
	 * @since    1.0.0
	 */
	public function add_options_page()
	{

		add_options_page(
			'Ciup Events Api',
			'Ciup Events Api',
			'manage_options',
			'ciup-events-api-plugin-admin',
			array($this, 'display_plugin_setup_page')
		);
	}

	/**
	 * Call the backoffice template
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_setup_page()
	{

		include_once('partials/ciup-events-api-plugin-admin-display.php');
	}

	/**
	 * Add settings action link to the plugins page.
	 * Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
	 *
	 * @since    1.0.0
	 */
	public function add_action_links($links)
	{
		$settings_link = array(
			'<a href="' . admin_url('options-general.php?page=ciup-events-api-plugin-admin') . '">' . __('Settings', 'ciup-events-api') . '</a>',
		);
		return array_merge($settings_link, $links);
	}

	/**
	 * Add thickbox to the wp backoffice
	 *
	 * @since    1.0.0
	 */
	function add_the_thickbox()
	{
		if (is_admin()) {
			add_thickbox();
		}
	}

	/**
	 * Add shortcode generator button in wp editor
	 *
	 * @since    1.0.0
	 */
	public function add_shortcode_button_to_editor()
	{
?>
		<!-- Button trigger modal -->
		<button type="button" class="modal-ciup-button button">
			<i class="icon-ciup-shortcode"></i>
			<?php _e('CIUP Shortcode', 'ciup-events-api-plugin'); ?>
		</button>
	<?php
	}

	/**
	 * Add the modal shortcode to the wp editor
	 *
	 * @since    1.0.0
	 */
	public function modal_shortcode_to_editor()
	{
	?>
		<!-- The Modal -->
		<div id="modal-ciup" class="modal">
			<!-- Modal content -->
			<div class="modal-content">
				<span class="modal-ciup-close">&times;</span>
				<div class="modal-header">
					<h1><?php _e('Generate the CIUP shortcode', 'ciup-events-api-plugin'); ?></h1>
					<p><?php _e('If you like to see the places and themes, check this link :', 'ciup-events-api-plugin'); ?>
						<a href="<?php echo get_admin_url() . 'options-general.php?page=ciup-events-api-plugin-admin'; ?>" target="_blank"><?php echo get_admin_url() . 'options-general.php?page=ciup-events-api-plugin-admin'; ?></a>
					</p>
				</div>
				<div class="modal-form">
					<div class="modal-form-line">
						<label class="modal-form-line-label" for="header"><?php _e('Do you want to show the header ?', 'ciup-events-api-plugin'); ?></label>
						<select id="header" class="modal-form-line-input">
							<option value="true"><?php _e('True', 'ciup-events-api-plugin'); ?></option>
							<option value="false"><?php _e('False', 'ciup-events-api-plugin'); ?></option>
						</select>
					</div>
					<div class="modal-form-line">
						<label class="modal-form-line-label" for="footer"><?php _e('Do you want to show the footer ?', 'ciup-events-api-plugin'); ?></label>
						<select id="footer" class="modal-form-line-input">
							<option value="true"><?php _e('True', 'ciup-events-api-plugin'); ?></option>
							<option value="false"><?php _e('False', 'ciup-events-api-plugin'); ?></option>
						</select>
					</div>
					<div class="modal-form-line">
						<label class="modal-form-line-label" for="layout"><?php _e('Choose your layout disposition :', 'ciup-events-api-plugin'); ?></label>
						<select id="layout" class="modal-form-line-input">
							<option value="default"><?php _e('Default', 'ciup-events-api-plugin'); ?></option>
							<option value="grid-2">Grid-2</option>
							<option value="grid-3">Grid-3</option>
							<option value="grid-4">Grid-4</option>
							<option value="grid-5">Grid-5</option>
							<option value="grid-6">Grid-6</option>
						</select>
					</div>
					<div class="modal-form-line">
						<label class="modal-form-line-label" for="elements"><?php _e('Number of elements to show ?', 'ciup-events-api-plugin'); ?></label>
						<input id="elements" class="modal-form-line-input" type="number" min="1" max="30" name="elements" value="4" />
					</div>
					<div class="modal-form-line">
						<label class="modal-form-line-label" for="place"><?php _e('Slug of your place ?', 'ciup-events-api-plugin'); ?></label>
						<input id="place" class="modal-form-line-input" type="text" name="place" placeholder="college-espagne" />
					</div>
					<div class="modal-form-line">
						<label class="modal-form-line-label" for="theme"><?php _e('Slug of your theme ?', 'ciup-events-api-plugin'); ?></label>
						<input id="theme" class="modal-form-line-input" type="text" name="theme" placeholder="exposition" />
					</div>
					<div class="modal-form-line">
						<label class="modal-form-line-label" for="period"><?php _e('Do you want to show past or future events ?', 'ciup-events-api-plugin'); ?></label>
						<select id="period" class="modal-form-line-input">
							<option value="future"><?php _e('Future', 'ciup-events-api-plugin'); ?></option>
							<option value="past"><?php _e('Past', 'ciup-events-api-plugin'); ?></option>
						</select>
					</div>
					<button class="button button-primary button-large create-shortcode"><?php _e('Create shortcode', 'ciup-events-api-plugin'); ?></button>
				</div>
				<div style="display: none;" class="ciup-shortcode-result">
					<h2 class="ciup-result-title"><?php _e('Result :', 'ciup-events-api-plugin'); ?></h2>
					<h3 class="shortcode-result"></h3>
					<button class="button button-primary button-large insert-shortcode"><?php _e('Insert shortcode', 'ciup-events-api-plugin'); ?></button>
				</div>
			</div>
		</div>
<?php
	}

	/**
	 * Create the ciup shortcode to be inserted in Tinymce block.
	 *
	 * @since    1.0.0
	 */
	public function create_the_shortcode($args)
	{
		$shortcode = "[ciup ";
		$shortcode_end = "]";

		$args = $_GET['params'];

		foreach ($args as $key => $value) {

			if ($key == 'layout' || $key == 'place' || $key == 'theme' || $key == 'period') {
				$value = "'" . $value . "'";
			}

			$shortcode = $shortcode . $key . "=" . $value . " ";
		}

		$shortcode = $shortcode . $shortcode_end;

		echo $shortcode;
		exit;
	}

	/**
	 * Get all taxonomies slug
	 *
	 * @since    1.0.0
	 */
	public static function get_all_taxonomies_slugs($cpt_url)
	{

		// Get the response from the wp api
		$url = CIUP_URL . 'wp-json/wp/v2/' . $cpt_url . '?per_page=100';
		$args = array();

		$response = wp_remote_get($url, $args);
		$res = json_decode(wp_remote_retrieve_body($response), true);

		// Init a new array
		$new_array = array();

		// Retrieve data for all themes
		foreach ($res as $one_res) {
			$cpt_slug = is_array($one_res) ? $one_res['slug'] : FALSE;
			$cpt_slug && array_push($new_array, $cpt_slug);
		}

		return $new_array;
	}
}
