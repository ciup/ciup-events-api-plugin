<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.leadoff.fr
 * @since      1.0.0
 *
 * @package    Ciup_Events_Api
 * @subpackage Ciup_Events_Api/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="ciup-backoffice-wrap">

    <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
    
    <div class="ciup-backoffice-explanation">
        <h2><?php _e( 'What is a shortcode ?', 'ciup-events-api-plugin' ); ?></h2>
        <p><?php _e( 'A shortcode is a specially formatted text tag that can be placed directly in an article or page on your blog. This tag is automatically interpreted by WordPress and allows you to add functionality (an image gallery, a video insertion, data.) without programming on your part.', 'ciup-events-api-plugin' ); ?></p>
        <h2><?php _e( 'How to use the ', 'ciup-events-api-plugin' ); ?><strong>CIUP</strong> shortcode ?</h2>
        <div class="ciup-backoffice-explanation-step">
            <button type="button" class="collapsible-button"><?php _e( 'Step 1 - Modify page', 'ciup-events-api-plugin' ); ?></button>
            <div class="collapsible-content">
                <p><?php _e( 'Go to the page you want to modify.', 'ciup-events-api-plugin' ); ?></p>
                <img src="<?php echo plugin_dir_url( __DIR__ ).'media/modify-page-step-1.png'; ?>" alt="modify-page-step-1">
            </div>
        </div>
        <div class="ciup-backoffice-explanation-step">
            <button type="button" class="collapsible-button"><?php _e( 'Step 2 - CIUP Shortcode button', 'ciup-events-api-plugin' ); ?></button>
            <div class="collapsible-content">
                <p><?php _e( 'You will see the CIUP Shortcode button, click it ! A modal will open with a form inside.', 'ciup-events-api-plugin' ); ?></p>
                <img src="<?php echo plugin_dir_url( __DIR__ ).'media/modal-shortcode-step-2.png'; ?>" alt="modal-shortcode-step-2">
            </div>
        </div>
        <div class="ciup-backoffice-explanation-step">
            <button type="button" class="collapsible-button"><?php _e( 'Step 3 - Configuration', 'ciup-events-api-plugin' ); ?></button>
            <div class="collapsible-content">
                <p><?php _e( 'Choose the configuration, and click on', 'ciup-events-api-plugin' ); ?> <span class="button button-primary button-large"><?php _e( 'Create shortcode', 'ciup-events-api-plugin' ); ?></span></p>
            </div>
        </div>
        <div class="ciup-backoffice-explanation-step">
            <button type="button" class="collapsible-button"><?php _e( 'Step 4 - Result', 'ciup-events-api-plugin' ); ?></button>
            <div class="collapsible-content">
                <p><?php _e( 'Wait a few seconds and the block result will shop up ! You can either copy/paste in the text or click on', 'ciup-events-api-plugin' ); ?> <span class="button button-primary button-large"><?php _e( 'Insert shortcode', 'ciup-events-api-plugin' ); ?></span><?php _e( ', this will add the shortcode automatically in the TinyMCE content where your cursor was last seen.', 'ciup-events-api-plugin' ); ?></p>
                <img src="<?php echo plugin_dir_url( __DIR__ ).'media/result-shortcode-step-3.png'; ?>" alt="result-shortcode-step-3">
            </div>
        </div>
        <div class="ciup-backoffice-explanation-step">
            <button type="button" class="collapsible-button"><?php _e( 'Step 5 - Update page', 'ciup-events-api-plugin' ); ?></button>
            <div class="collapsible-content">
                <p><?php _e( 'Update your page and that\'s it ! Congratulations !', 'ciup-events-api-plugin' ); ?></p>
                <img src="<?php echo plugin_dir_url( __DIR__ ).'media/insert-text-step-4.png'; ?>" alt="insert-text-step-4">
            </div>
        </div>
    </div>
    <div class="ciup-backoffice-list-wrap">
        <h3><?php _e( 'List of themes slugs', 'ciup-events-api-plugin' ); ?></h3>
        <ul class="ciup-backoffice-list">
            <?php 
                $themes = Ciup_Events_Api_Admin::get_all_taxonomies_slugs('themes');
                foreach ($themes as $one_theme) {
                    ?>
                        <li class="ciup-backoffice-list-item"><?php echo $one_theme; ?></li>
                    <?php
                }
            ?>
        </ul>
    </div>
    <div class="ciup-backoffice-list-wrap">
        <h3><?php _e( 'List of places slugs', 'ciup-events-api-plugin' ); ?></h3>
        <ul class="ciup-backoffice-list">
            <?php 
                $places = Ciup_Events_Api_Admin::get_all_taxonomies_slugs('places');
                foreach ($places as $one_place) {
                    ?>
                        <li class="ciup-backoffice-list-item"><?php echo $one_place; ?></li>
                    <?php
                }
            ?>
        </ul>
    </div>
</div>