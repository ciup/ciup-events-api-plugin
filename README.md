# CIUP Events API

CIUP Events API is a Wordpress plugin. CIUP API is our way to received data from the CIUP website.

## Installation

The WP plugin is dependant to **JQuery**. Do not forget to import the JQuery Library.

1. Install the [CIUP EVENTS API Plugin](https://gitlab.com/ciup/ciup-events-api-plugin)
2. Activate the **CIUP EVENTS API Plugin** !
3. Need some help ? Check the  [documentation](https://gitlab.com/ciup/api) of the plugin.

## List of themes and places

### Themes 

|  |  |  |  |  |  |
|--|--|--|--|--|--|
| cinema | conference-debat | exposition | festival | innovation | insolite |
| jeune-public | musique | rencontre | sport | theatre | visites-guidees |

### Places

|  |  |  |  |
|--|--|--|--|
| bibliotheque | college-espagne | college-franco-britannique | college-neerlandais | 
| comptoir-coreen | espace-exposition-plein-air | fondation-abreu-de-grancher | fondation-avicenne| 
|fondation-biermans-lapotre | fondation-danoise | fondation-chine  | fondation-monaco  |
| fondation-etats-unis | fondation-deutsch-de-la-meurthe | fondation-hellenique | fondation-lucien-paye |
| fondation-suisse | fondation-victor-lyon | oblique | maison-argentine |
| maison-ile-de-france | maison-inde | maison-italie | maison-coree |
| maison-tunisie | maison-tunisie-pavillon-habib-bourguiba | maison-norvege | maison-eleves-ingenieurs-arts-et-metiers |
| maison-etudiants-armeniens | maison-etudiants-canadiens | maison-etudiants-asie-du-sud-est | maison-etudiants-francophonie |
| maison-etudiants-suedois | maison-industries-agricoles-alimentaires | maison-provinces-france | maison-bresil |
| maison-cambodge | maison-japon | maison-liban | maison-maroc |
| maison-mexique | maison-portugal | maison-heinrich-heine | maison-internationale |
| maison-internationale-agroparistech | miksi-coworking | parc-cite-internationale | residence-andre-honnorat |
| residence-julie-victoire-daubie | residence-lila | residence-robert-garric | restaurant-universitaire |
| salle-polyvalente | studios-musique | theatre-cite-internationale |  |

## Bugs and feature requests

If you find a bug, please report [it here on Github.](https://gitlab.com/ciup/api/issues/new)

Guidelines for bug reports:

Use the GitHub issue search — check if the issue has already been reported.
Check if the issue has been fixed — try to reproduce it using the latest master branch in the repository.
A good bug report shouldn't leave others needing to chase you up for more information. Please try to be as detailed as possible in your report.

Feature requests are welcome. Please look for existing ones and use GitHub's "reactions" feature to vote.

## License

[CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)