<?php

/**
 * @link              https://gitlab.com/ciup/ciup-events-api-plugin
 * @since             1.0.0
 * @package           Ciup_Events_Api
 *
 * @wordpress-plugin
 * Plugin Name:       CIUP Events API
 * Plugin URI:        http://www.leadoff.fr
 * Description:       CIUP Events API is a Wordpress plugin. CIUP Events API is our way to received data from the CIUP Website.
 * Version:           1.0.0
 * Author:            LEAD OFF
 * Author URI:        http://www.leadoff.fr
 * License:           CC BY-NC-SA 4.0
 * License URI:       https://creativecommons.org/licenses/by-nc-sa/4.0/
 * Text Domain:       ciup-events-api-plugin
 * Domain Path:       /languages
 * GitLab Plugin URI: https://gitlab.com/ciup/ciup-events-api-plugin
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CIUP_EVENTS_API_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-ciup-events-api-plugin-activator.php
 */
function activate_ciup_events_api() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ciup-events-api-plugin-activator.php';
	Ciup_Events_Api_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-ciup-events-api-plugin-deactivator.php
 */
function deactivate_ciup_events_api() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-ciup-events-api-plugin-deactivator.php';
	Ciup_Events_Api_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_ciup_events_api' );
register_deactivation_hook( __FILE__, 'deactivate_ciup_events_api' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-ciup-events-api-plugin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_ciup_events_api() {

	$plugin = new Ciup_Events_Api();
	$plugin->run();

}
run_ciup_events_api();
